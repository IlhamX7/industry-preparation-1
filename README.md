# Backend Sparepart Item with Express.js and MongoDB

## Requirements
For running this project please install nodejs and mongodb.
NodeJS:
```
node --version
v14.17.6
```
MongoDB:
```
mongo --version
MongoDB shell version v4.4.4
```

## Getting Started
Install npm package:
```
npm install
```

Run the server:
```
npm run dev
```

## Steps :
- Clone this repo
- Install dependencies
- Running project on development 